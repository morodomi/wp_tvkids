<div id="breadcrumb">
  <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
  <a href="<?php echo home_url(); ?>" itemprop="url"> <span itemprop="title"><?php bloginfo('title'); ?></span> </a> &gt;
  </div>
  <?php if(is_category()) : ?>
    <?php
      $postcat = get_the_category();
      $catid = $postcat[0]->cat_ID;
      $allcats = array($catid);
      while($catid == 0) {
        $mycat = get_category($catid);
        $catid = $mycat->parent;
        array_push($allcats, $catid);
      }
      array_pop($allcats);
      $allcats = array_reverse($allcats);
      foreach($allcats as $catid) :
    ?>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="<?php echo get_category_link($catid); ?>" itemprop="url"><span itemprop="title"><?php echo get_cat_name($catid); ?></span></a> &gt;</div>
    <?php endforeach; ?>
  <?php elseif(is_tax()) : ?>
    <?php
      $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
      $termid = $term->term_id;
      $allterms = array($termid);
      while($termid == 0) {
        $myterm = get_term_by('id', $termid, get_query_var('taxonomy'));
        $termid = $myterm->parent;
        array_push($allterms, $termid);
      }
      $allterms = array_reverse($allterms);
      foreach($allterms as $termid) :
        $term = get_term_by('id', $termid, get_query_var('taxonomy'));
    ?>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb"> <a href="<?php echo get_term_link($term); ?>" itemprop="url"><span itemprop="title"><?php echo $term->name ; ?></span></a> &gt;</div>
    <?php endforeach; ?>

  <?php else : ?>
  <?php endif; ?>
</div><!-- /#breadcrumb -->
