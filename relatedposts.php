<?php
/*
 * 関連リンク
 */
global $post;
$args = array(
  'posts_per_page' => 8,
  'post_type' => 'post',
  'orderby' => 'rand',
  'post_status' => 'publish',
  'post__not_in' => array($post->ID),
);
$related_links = new WP_Query($args);
if($related_links->have_posts()) :
?>
<div id="related-posts">
  <h3 id="related-posts-title">関連リンク</h3>
  <?php while($related_links->have_posts()): $related_links->the_post(); ?>
    <div class="related-post">
      <a href="<?php the_permalink(); ?>">
        <div class="related-post-description">
          <h4><?php the_title(); ?></h4>
          <?php
            $value = get_post_meta(get_the_ID(), 'start-time', true);
            if($value) {
              echo '<div class="start-time">'.$value.'</div>';
            }
          ?>
        </div>
        <?php the_post_thumbnail('thumb100'); ?>
      </a>
    </div>
  <?php endwhile; ?>
</div><!-- /#related-posts -->
<?php
endif;
wp_reset_postdata();
?>
