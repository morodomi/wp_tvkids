<?php
  // サイドバー表示
?>
<div id="side">
  <aside>
    <?php if(is_active_sidebar('default_dynamic_sidebar')) : ?>
    <ul>
    <?php dynamic_sidebar('default_dynamic_sidebar'); ?>
    </ul>
    <?php endif; ?>
  </aside>
</div>

