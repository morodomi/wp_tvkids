<div id="comments">
  <?php if(have_comments()) : ?>
  <h3 id="resp">コメント</h3>
  <ol class="comments-list">
    <?php wp_list_comments('avatar_size=55'); ?>
  </ol>
  <?php endif; ?>
  <?php
    $args = array(
      'title_reply' => 'メッセージ',
      'label_submit' => ('送信')
    );
    comment_form($args);
  ?>
</div>
