<?php
/*
 * 初期化関数
 */

// 文末文字の変更
function custom_excerpt_more($more) {
  return ' ... ';
}
add_filter('excerpt_more', 'custom_excerpt_more');

// 検索フォームをHTML5にする
add_theme_support('html5', array('search-form'));

function change_posts_per_page($query) {
  if(is_admin() || !$query->is_main_query) {
    return;
  }
  $query->set('meta_key', 'start-time');
  $query->set('orderby', 'meta_value');
  $query->set('order', 'DESC');
}
add_action('pre_get_posts', 'change_posts_per_page');

# headerで消すもの
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'rest_output_link_wp_head', 10 );
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10 );
