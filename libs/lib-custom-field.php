<?php
/*
 * 放送開始時間の追加
 */
function add_start_time_field() {
  add_meta_box('start-time', '放送開始時間', 'add_start_time_box', 'post', 'advanced', 'default', NULL);
}
function add_start_time_box($post) {
  $value = get_post_meta($post->ID, 'start-time', true);
  echo '放送開始時間を入力して下さい。<br>';
  echo '<input type="text" name="start-time" class="datetimepicker" value="'.esc_attr($value).'">';
  wp_nonce_field('time_key', 'time_nonce');
}
function save_start_time_field($post_id) {
  if(!isset($_POST['time_nonce']) || !check_admin_referer('time_key','time_nonce') || !current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  update_post_meta($post_id, 'start-time', $_POST['start-time']);
  return $post_id;
}
add_action('add_meta_boxes', 'add_start_time_field');
add_action('save_post','save_start_time_field');

function manage_posts_columns($columns) {
  $columns['channel'] = 'チャンネル';
  $columns['period'] = '時間帯';
  $columns['start-time'] = '開始時間';
  $columns['weekday'] = '曜日';
  $columns['target-age'] = '対象年齢';
  return $columns;
}
function add_columns($column_name, $post_id) {
  switch($column_name) {
  case 'channel':
  case 'period':
  case 'weekday':
  case 'target-age':
    $terms = wp_get_object_terms($post_id, $column_name, array('orderby' => 'id'));
    $output = array();
    foreach($terms as $term) {
      $link = get_term_link($term->term_id, $column_name);
      $output[] = '<a href="'.$link.'">'.$term->name.'</a>';
    }
    echo implode(' /', $output);
    break;
  case 'start-time':
    $meta = get_post_meta($post_id, $column_name, true);
    if($meta) {
      echo $meta;
    } else {
      echo '-';
    }
    break;

  default:
    echo __('None');
  }
}
add_filter('manage_edit-post_columns', 'manage_posts_columns');
add_action('manage_posts_custom_column', 'add_columns', 10, 4);
