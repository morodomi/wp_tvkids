<?php
/*
 * チャンネル一覧追加
 */
function taxonomy_channel() {
  register_taxonomy(
    'channel',
    'post',
    array(
      'hierarchial' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'チャンネル',
      'singular_label' => 'チャンネル',
      'public' => true,
      'show_ui' => true
    )
  );
  wp_insert_term('NHK', 'channel', array('slug' => 'nhk'));
  wp_insert_term('日本テレビ', 'channel', array('slug' => 'ntv'));
  wp_insert_term('テレビ朝日', 'channel', array('slug' => 'tv-asahi'));
  wp_insert_term('TBS', 'channel', array('slug' => 'tbs'));
  wp_insert_term('フジテレビ', 'channel', array('slug' => 'fuji-tv'));
  wp_insert_term('テレビ東京', 'channel', array('slug' => 'tv-tokyo'));
  wp_insert_term('東京MX', 'channel', array('slug' => 'tokyo-mx'));
}
/*
 * 時間帯一覧追加
 */
function taxonomy_period() {
  register_taxonomy(
    'period',
    'post',
    array(
      'hierarchial' => false,
      'update_count_callback' => '_update_post_term_count',
      'label' => '時間帯',
      'singular_label' => '時間帯',
      'public' => true,
      'show_ui' => true
    )
  );
  wp_insert_term('午前', 'period', array('slug' => 'am'));
  wp_insert_term('午後', 'period', array('slug' => 'pm'));
  wp_insert_term('6 - 9時', 'period', array('slug' => 'evening'));
  wp_insert_term('10時以降', 'period', array('slug' => 'night'));
}

/*
 * 曜日
 */
function taxonomy_weekday() {
  register_taxonomy(
    'weekday',
    'post',
    array(
      'hierarchial' => false,
      'update_count_callback' => '_update_post_term_count',
      'label' => '曜日',
      'singular_label' => '曜日',
      'public' => true,
      'show_ui' => true
    )
  );
  wp_insert_term('日曜日', 'weekday', array('slug' => 'sunday'));
  wp_insert_term('月曜日', 'weekday', array('slug' => 'monday'));
  wp_insert_term('火曜日', 'weekday', array('slug' => 'tuesday'));
  wp_insert_term('水曜日', 'weekday', array('slug' => 'wednesday'));
  wp_insert_term('木曜日', 'weekday', array('slug' => 'thursday'));
  wp_insert_term('金曜日', 'weekday', array('slug' => 'friday'));
  wp_insert_term('土曜日', 'weekday', array('slug' => 'saturday'));
}

function taxonomy_tareget_age() {
  register_taxonomy(
    'target-age',
    'post',
    array(
      'hierarchial' => false,
      'update_count_callback' => '_update_post_term_count',
      'label' => '対象年齢',
      'singular_label' => '対象年齢',
      'public' => true,
      'show_ui' => true
    )
  );
  wp_insert_term('- 3歳', 'target-age', array('slug' => 'under-3-years'));
  wp_insert_term('4 - 6歳', 'target-age', array('slug' => 'under-6-years'));
  wp_insert_term('低学年', 'target-age', array('slug' => 'under-9-years'));
  wp_insert_term('高学年', 'target-age', array('slug' => 'under-12-years'));
  wp_insert_term('中高生', 'target-age', array('slug' => 'highscrool'));
  wp_insert_term('一般', 'target-age', array('slug' => 'general'));
}

function taxonomy_genre() {
  register_taxonomy(
    'genre',
    'post',
    array(
      'hierarchial' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'ジャンル',
      'singular_label' => 'ジャンル',
      'public' => true,
      'show_ui' => true
    )
  );
  wp_insert_term('算数', 'genre', array('slug' => 'math'));
  wp_insert_term('科学', 'genre', array('slug' => 'science'));
  wp_insert_term('生物', 'genre', array('slug' => 'biology'));
  wp_insert_term('地理', 'genre', array('slug' => 'geography'));
  wp_insert_term('歴史', 'genre', array('slug' => 'history'));
  wp_insert_term('英語', 'genre', array('slug' => 'english'));
  wp_insert_term('海外', 'genre', array('slug' => 'abroad'));
  wp_insert_term('日本', 'genre', array('slug' => 'japan'));
  wp_insert_term('体育', 'genre', array('slug' => 'sports'));
  wp_insert_term('音楽', 'genre', array('slug' => 'music'));
  wp_insert_term('美術', 'genre', array('slug' => 'art'));
  wp_insert_term('技術', 'genre', array('slug' => 'tech'));
  wp_insert_term('雑学', 'genre', array('slug' => 'knowledge'));
}

function taxonomy_series() {
  register_taxonomy(
    'series',
    'post',
    array(
      'hierarchial' => false,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'シリーズ',
      'singular_label' => 'シリーズ',
      'public' => true,
      'show_ui' => true
    )
  );
}

function init_category() {
  wp_insert_term('トピックス', 'category', array('slug' => 'topics'));
}

function init_taxonomies() {
  init_category();
  taxonomy_channel();
  taxonomy_period();
  taxonomy_weekday();
  taxonomy_tareget_age();
  taxonomy_genre();
  taxonomy_series();
}

add_action('init', 'init_taxonomies');
