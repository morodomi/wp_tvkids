<?php
  // サイドバーウィジェット登録
  register_sidebar(
    array(
      'id' => 'default_dynamic_sidebar',
      'name' => 'サイドバー',
      'before_widget' => '<li>',
      'after_widget' => '</li>',
      'before_title' => '<h2 class="widget-title">',
      'after_title' => '</h2>'
    )
  );
  // ヘッダーウィジェット追加
  register_sidebar(
    array(
      'id' => 'default_dynamic_header',
      'name' => 'ヘッダーウィジェット',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '<h2 class="header-widget-title">',
      'after_title' => '</h2>'
    )
  );
  // フッターウィジェット追加
  register_sidebar(
    array(
      'id' => 'default_dynamic_footer',
      'name' => 'フッターウィジェット',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '<h2 class="footer-widget-title">',
      'after_title' => '</h2>'
    )
  );

