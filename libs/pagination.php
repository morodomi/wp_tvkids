<?php
/*
 * ページャー機能
 */
function pagination($pages = '', $range = 4) {
  $showitems= ($range * 2) + 1;
  global $paged;
  if(empty($paged)) $paged = 1;
  if($pages == '') {
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if(!$pages) {
      $pages = 1;
    }
  }
  if($pages != 1) {
    echo '<ul id="pagination">';
    if($paged > 1 && $showitems < $pages) {
      echo "<li><a href='".get_pagenum_link($paged - 1)."'>前へ</a></li>";
    }
    if($paged > 2 && $paged > $range + 1 && $showitems < $pages) {
      echo  "<li><a href='".get_pagenum_link(1)."'>1</a></li>";
    }

    for($i = 1; $i <= $pages; $i++) {
      if($pages != 1 && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
        echo '<li><a href="'.get_pagenum_link($i).'" class="'.(($paged == $i) ? 'active' : 'inactive').'">'.$i.'</a></li>';
      }
    }

    if($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) {
      echo '<li><a href="'.get_pagenum_link($pages).'">'.$i.'</a></li>';
    }
    echo '</ul>';
  }
}
