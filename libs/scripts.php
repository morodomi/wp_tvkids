<?php
// スクリプトファイル読み込み
function load_javascripts() {
  if(!is_admin()) {
    $dir = get_stylesheet_directory_uri();
    wp_enqueue_style('tvkids-css',$dir.'/style.css');
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
//    wp_deregister_script('jquery');
//    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js');
    wp_enqueue_script('tvkids-script',$dir.'/js/script.js',array('jquery'), '1.0', true);
  }
}
add_action('wp_enqueue_scripts','load_javascripts',50);

function admin_javascripts() {
  $dir = get_stylesheet_directory_uri();
  wp_enqueue_style('datetimepicker-style', $dir.'/css/jquery.datetimepicker.css');
//  wp_deregister_script('jquery');
//  wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js');
  wp_enqueue_script('datetimepicker-script', $dir.'/js/jquery.datetimepicker.full.min.js');
  wp_enqueue_script('admin-script', $dir.'/js/admin.js');
}
add_action('admin_enqueue_scripts','admin_javascripts',50);
