<?php
/*
 * 画像系functions.php
 */
// サムネイルサポート
add_theme_support('post-thumbnails');
add_image_size('thumb50',50,50,true);
add_image_size('thumb100',100,100,true);
add_image_size('thumb150',150,150,true);

// 画像をレスポンシブにする
function filter_images($html) {
  // img-responsiveクラス追加
  $classes = 'img-responsive';
  if(preg_match('/<img.*? class="/', $html)) {
    $html = preg_replace('/(<img.*? class=".*?)(".*?\/>)/', '$1 ' . $classes . ' $2', $html);
  } else {
    $html = preg_replace('/(<img.*?)(\>)/', '$1 class="' . $classes . '" $2', $html);
  }
  $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
  // 画像リンク削除
  $html = preg_replace(array('{<a(.*?)(wp-att|wp-content/uploads)[^>]*><img}','{ wp-image-[0-9]*" /></a>}'),array('<img','"/>'), $html);
  return $html;
}
add_filter('the_content', 'filter_images', 10);
add_filter('post_thumbnail_html', 'filter_images', 10);
