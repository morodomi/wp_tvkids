<?php get_header(); ?>
<div id="content" class="clearfix">
  <div id="content-inner">
    <main>
      <article>
        <?php get_template_part('breadcrumb'); ?>
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
          <div id="entry">
            <h1 id="entry-title">
              <?php the_title(); ?>
            </h1>
            <div id="taxonomy">
            <?php
              $value = get_post_meta(get_the_ID(), 'start-time', true);
              if($value) {
                echo '<span id="start-time">'.$value.' 放送</span>';
              }
            ?>
            <?php
              $taxonomies = array('channel', 'genre', 'target-age');
              foreach($taxonomies as $taxonomy) {
                $values = wp_get_post_terms(get_the_ID(), $taxonomy, array('orderby' => 'id', 'order' => 'ASC'));
                if($values) {
                  echo '<ul id="'.$taxonomy.'">';
                  foreach($values as $value) {
                    echo '<li class="term"><a href="'.get_category_link($value->term_id).'">'.$value->name.'</a></li>';
                  }
                  echo '</ul>';
                  if($taxonomy === 'target-age') {
                    echo '向け';
                  }
                }
              }
            ?>
            </div>
            <div id="thumbnail">
            <?php
              if(has_post_thumbnail(get_the_ID())) {
                the_post_thumbnail('full');
              }
            ?>
            </div>
            <div id="entry-body">
              <?php the_content(); ?> 
            </div>
          </div><!-- /#entry -->
          <?php get_template_part('relatedposts'); ?>
        <?php endwhile; else : ?>
          <p>記事がありません</p>
        <?php endif; ?>
        <?php
          // コメント
          if(comments_open() || get_comments_number()) {
            comments_template();
          }
        ?>
      </article>
    </main>
  </div><!-- /#content-inner -->
  <?php get_sidebar(); ?>
</div><!-- /#content -->
<?php get_footer(); ?>
