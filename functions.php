<?php

require_once locate_template('libs/init.php');
require_once locate_template('libs/debug.php');
require_once locate_template('libs/scripts.php');
require_once locate_template('libs/lib-images.php');
require_once locate_template('libs/lib-taxonomy.php');
require_once locate_template('libs/lib-custom-field.php');
require_once locate_template('libs/lib-widget.php');
require_once locate_template('libs/pagination.php');

