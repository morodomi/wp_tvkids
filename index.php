<?php get_header(); ?>
<div id="content" class="clearfix">
  <div id="content-inner">
    <main>
      <article>
        <?php get_template_part('breadcrumb'); ?>
        <section>
          <?php get_template_part('list'); ?>
        </section><!-- /section -->
        <?php if(function_exists('pagination')) {
          pagination($wp_query->max_num_pages);
        } ?>
      </article>
    </main>
  </div>
  <?php get_sidebar(); ?>
</div><!-- /#content -->
<?php get_footer(); ?>
