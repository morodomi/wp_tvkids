<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <meta charset="<?php bloginfo('charset') ?>">
  <meta name="format-detection" content="telephone=no,address=no,email=no">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="chrome=1,IE=Edge">
  <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
  <meta name="author" content="子どもと見るテレビ">
  <?php if(is_home() || is_front_page()) : ?>
    <meta name="description" content="教育の観点から、オススメの子どもと見るテレビ番組を紹介します。">
  <?php endif; ?>
  <?php /* Open Graph Tags */ ?>
  <meta property="og:title" content="<?php wp_title('|', true, 'right'); bloginfo('name'); ?>">
  <meta property="og:type" content="website">
  <meta property="og:site_name" content="子どもと見るテレビ">
  <meta property="og:url" content="<?php echo the_permalink(); ?>">
  <?php /* thumbnail image */ ?>
  <?php
    $image_id = get_post_thumbnail_id();
    $image_url = wp_get_attachment_image_src($image_id, true);
    if(is_array($image_url)) {
      $image_url = $image_url[0];
    }   
  ?>  
  <meta property="og:image" content="<?php echo $image_url ?>">
  <?php
    // canonical url
    global $page, $paged, $wp_query;
    if(is_home() || is_front_page()) {
      $canonical_url = "http://tvkids.com/";
    } elseif(is_category()) {
      $canonical_url = get_category_link(get_query_var('cat'));
    } elseif(is_page() || is_single()) {
      $canonical_url = get_permalink();
      if ($paged >= 2 || $page >= 2) {
        $canonical_url = $canonical_url.'page/'.max($paged,$page).'/';
      }   
    } elseif(is_tag()) {
      $encodde_tag = urlencode(single_tag_title('',false));
      $canonical_url = "http://tvkids.com/archives/tag/".$encodde_tag;
    } else {
      $canonical_url = null;
    }
    if($canonical_url == !null) {
  ?>
  <link rel="canonical" href="<?php echo $canonical_url; ?>"/>
  <?php } ?>
<?php if(is_category()): ?>
<?php elseif(is_archive() || is_search() || is_tag() || is_paged()): ?>
  <meta name="robots" content="noindex,follow">
<?php endif; ?>
  <title>
<?php
  if(is_front_page()):
  elseif(is_single() || is_page() || is_archive() || is_search()):
    wp_title(' | ', true, 'right');
  elseif(is_404()):
    echo '404 |';
  endif;
  bloginfo('name');
  if($paged >= 2 || $page >= 2):
    echo '-'.sprintf('%sページ',max($paged,$page));
  endif;
?>
  </title>
  <?php /* FaviconとApple icon設定 */ ?>
  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" sizes="16x16 32x32 48x48">
  <?php wp_head(); ?>
</head>
<body>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-75335473-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
  </script>
  <div class="container">
  <header>
    <div class="fixbar">
      <h1><a href="<?php home_url(); ?>/">子どもと見るテレビ</a></h1>
      <div class="sp-header-menu">
        <i id="sp-header-menu-icon" class="fa fa-bars">
        </i>
      </div>
      <div class="header-menu">
      <?php
        $taxonomies = array(
          'genre' => 'ジャンル',
          'target-age' => '対象年齢',
          'channel' => 'チャンネル',
          'weekday' => '放送曜日'
        );
        foreach($taxonomies as $taxonomy => $name) {
          $args = array(
            'orderby' => 'id',
            'order' => 'ASC',
            'pad_counts' => true,
            'hide_empty' => true
          );
          $terms = get_terms($taxonomy, $args);
          if(count($terms) != 0) {
            echo '<div class="dropdown"><span>'.$name.'</span><div class="dropdown-content">';
            foreach($terms as $term) {
              echo '<a href="'.get_term_link($term).'">'.$term->name.'</a>';
            } 
            echo '</div></div><!-- /.dropdown -->';
          }
        }
      ?>
      </div><!-- /.header-menu -->
      <?php get_search_form(); ?>
    </div>
    <div class="header-top">
      <p>子どもと見ることができる、おすすめテレビ番組を紹介します。</p>
      
    </div>
  </header>
