<?php get_header(); ?>
<div id="content" class="clearfix">
  <div id="content-inner">
    <main>
      <article>
        <?php get_template_part('breadcrumb'); ?>
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
          <div id="entry">
            <h1 id="entry-title">
              <?php the_title(); ?>
            </h1>
            <div id="taxonomy">
            <?php
              $value = get_post_meta(get_the_ID(), 'start-time', true);
              if($value) {
                echo $value.' 放送';
              }
            ?>

            </div>
            <div id="thumbnail">
            <?php
              if(has_post_thumbnail(get_the_ID())) {
                the_post_thumbnail('full');
              }
            ?>
            </div>
            <div id="entry-body">
              <?php the_content(); ?> 
            </div>
          </div><!-- /#entry -->
        <?php endwhile; else : ?>
          <p>記事がありません</p>
        <?php endif; ?>
      </article>
    </main>
  </div><!-- /#content-inner -->
  <?php get_sidebar(); ?>
</div><!-- /#content -->
<?php get_footer(); ?>
