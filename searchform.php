<div id="search">
  <form method="get" id="searchform" action="<?php echo home_url(); ?>/">
    <input type="search" value="<?php the_search_query(); ?>" name="s" id="s"/>
    <input type="image" src="<?php echo get_template_directory_uri(); ?>/images/search.png" alt="検索" id="searchsubmit" value="検索"/>
    <input type="hidden" value="post" name="post_type" id="post_type" />
  </form>
</div>
