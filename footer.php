  </div><!-- /.container -->
<footer>
  <div class="taxonomy">
    <?php
      $taxonomies = array(
        'genre' => 'ジャンル',
        'target-age' => '対象年齢',
        'channel' => 'チャンネル',
        'weekday' => '放送曜日'
      );
      foreach($taxonomies as $taxonomy => $name) {
        $args = array(
          'orderby' => 'id',
          'order' => 'ASC',
          'pad_counts' => true,
          'hide_empty' => true
        );
        $terms = get_terms($taxonomy, $args);
        if(count($terms) != 0) {
          echo '<div class="footer-taxonomy"><span>'.$name.'</span><div class="term">';
          foreach($terms as $term) {
            echo '<a href="'.get_term_link($term).'">'.$term->name.'</a>';
          } 
          echo '</div></div><!-- /.footer-taxonomy-->';
        }
      }
    ?>
  </div>
  <div class="copyright">
    Copyright 2016 <a href="<?php home_url(); ?>"><?php echo bloginfo('name');?></a>
  </div> 
</footer>
<?php wp_footer(); ?>
</body>
</html>
