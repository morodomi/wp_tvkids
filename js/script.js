jQuery(function() {
  jQuery(window).on('scroll', function() {
    if(jQuery(this).scrollTop() > 50) {
      jQuery('header').addClass('fixed');
    } else {
      jQuery('header').removeClass('fixed');
    }
  });
  jQuery('#sp-header-menu-icon').on('click', function() {
    jQuery('html,body').animate({scrollTop:jQuery('footer').offset().top}, 500);
  });
});
