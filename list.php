<div id="post-list">
  <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
    <div class="post clearfix">
      <div class="post-description">
        <a href="<?php the_permalink() ?>">
          <?php 
//            if(mb_strlen(get_the_title()) > 30) {
//              echo mb_substr(get_the_title(), 0, 28) . ' ...';
//            } else {
              the_title();
//            }
          ?>
        </a>
        <div class="start-time">
          <?php
            $value = get_post_meta(get_the_ID(), 'start-time', true);
            if($value) {
              echo $value . ' 放送';
            }
          ?>
        </div>
        <div class="taxonomies">
          <?php
            $taxonomies = array('genre', 'channel', 'target-age');
            foreach($taxonomies as $taxonomy) {
              $terms = wp_get_object_terms(get_the_ID(), $taxonomy);
              $output = array();
              foreach($terms as $term) {
                $link = get_term_link($term->term_id, $taxonomy);
                $output[] = '<a href="'.$link.'">'.$term->name.'</a>';
              }
              echo '<div class="taxonomy">'.implode('/',$output).'</div>';
            }
          ?>
        </div>
      </div>
      <div class="post-thumbnail">
        <?php if(has_post_thumbnail(get_the_ID())) {
          the_post_thumbnail('thumb100');
        } ?>
      </div>
    </div>
  <?php endwhile; else : ?>
  <p>記事がありません</p>
  <?php endif; ?>
</div>
