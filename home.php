<?php get_header(); ?>
<div id="content" class="clearfix">
  <div id="content-inner">
    <main>
      <article>
      <?php
        // トップ2カラム
        $ids = array('pickup' => '注目番組', 'upcoming' => 'これから放送');
        $date = date('Y/m/d h:i');
        $args['pickup'] = array(
          'post_type' => 'post',
          'category_name' => 'topics',
          'orderby' => 'date',
          'order' => 'DESC',
          'posts_per_page' => 3,
        );
        $args['upcoming'] = array(
          'post_type' => 'post',
          'meta_key' => 'start-time',
          'orderby' => 'meta_value',
          'order' => 'ASC',
          'type' => 'DATE',
          'posts_per_page' => 3,
          'meta_query' => array(
            'key' => 'start-time',
            'value' => $date,
            'compare' => '>'
          )
        );
        foreach($ids as $id => $name) {
          echo '<div id="'.$id.'">';   
          echo '<h2 class="category-title">'.$name.'</h2>';
          echo '<div class="category-underline"></div>';
          $query = new WP_Query($args[$id]);
          if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
          ?>
            <div class="pickup-item">
              <div class="pickup-text">
                <h3><a href="<?php the_permalink(); ?>"><?php
                  if(mb_strlen(get_the_title()) > 30) {
                    echo mb_substr(get_the_title(), 0, 28) . ' ...';
                  } else {
                    the_title();
                  }
                ?></a></h3>
                <div class="pickup-description">
                  <?php $value = get_post_meta(get_the_ID(), 'start-time', true); ?>
                  <span class="start-time"><?php echo $value; ?></span>
                  <?php
                    $terms = wp_get_object_terms(get_the_ID(), 'genre');
                    $output = array();
                    foreach($terms as $term) {
                      $link = get_term_link($term->term_id, 'genre');
                      $output[] = '<a href="'.$link.'" class="taxonomy">'.$term->name.'</a>';
                    }
                    echo implode('/', $output);
                  ?>
                </div>
              </div>
              <div class="pickup-thumbnail">
              <?php
                if(has_post_thumbnail(get_the_ID())) {
                  the_post_thumbnail('thumb100');
                }
              ?>
              </div>
            </div>
            <?php endwhile; wp_reset_postdata(); ?>
          <?php else : ?>
            記事がありません。
          <?php endif;
          echo '</div><!-- /#'.$id.' -->';
        }
      ?>
      <?php
        // ジャンルごとの最新
        $genres = array(
          'biology' => '生物',
          'science' => '科学',
          'english' => '英語',
          'history' => '歴史',
          'tech' => '技術',
          'geography' => '地理',
          'math' => '数学',
          'sports' => '体育',
          'music' => '音楽',
          'art' => '美術',
          'abroad' => '海外',
          'japan' => '日本');
        foreach($genres as $genre => $name) {
          echo '<div id="'.$genre.'" class="genre">';
          echo '<h2 class="genre-title"><a href="'.get_term_link($genre, 'genre').'">'.$name.'</a></h2>';
          echo '<div class="genre-underline"></div>';
          $args = array(
            'post_type' => 'post',
            'meta_key' => 'start-time',
            'orderby' => 'meta_value',
            'order' => 'DESC',
            'posts_per_page' => 3,
            'tax_query' => array(
              array(
                'taxonomy' => 'genre',
                'field' => 'slug',
                'terms' => $genre
              )
            )
          );
          $query = new WP_Query($args);
          if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
          ?>
            <div class="genre-item">
              <div class="genre-text">
                <h3><a href="<?php the_permalink(); ?>"><?php
                  if(mb_strlen(get_the_title()) > 30) {
                    echo mb_substr(get_the_title(), 0, 28) . ' ...';
                  } else {
                    the_title();
                  }
                ?></a></h3>
                <?php $value = get_post_meta(get_the_ID(), 'start-time', true); ?>
                <p class="start-time"><?php echo $value; ?></p>
              </div>
              <div class="genre-thumbnail">
              <?php
                if(has_post_thumbnail(get_the_ID())) {
                  the_post_thumbnail('thumb50');
                }
              ?>
              </div>
            </div>
            <?php endwhile; wp_reset_postdata(); ?>
            <div class="read-more">
              <a href="<?php echo get_term_link($genre, 'genre'); ?>">もっと読む</a>
            </div>
          <?php else : ?>
            記事がありません。
          <?php endif;
          echo '</div><!-- /#'.$genre.' -->';
        }
      ?>
      </article>
    </main>
  </div><!-- /#content-inner -->
  <?php get_sidebar(); ?>
</div><!-- /#content -->
<?php get_footer(); ?>
